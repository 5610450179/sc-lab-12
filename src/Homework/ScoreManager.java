package Homework;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;



public class ScoreManager {

	private ArrayList<Student> students = new ArrayList<Student>();
	String scorename;

	public ScoreManager(String scorename){
		this.scorename = scorename;
	}

	public void readFile(String file) {
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader buffer = new BufferedReader(fileReader);

			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				String name = data[0].trim();
				double average = 0;
				for(int i=1;i<data.length;i++){
					average += Double.parseDouble(data[i].trim());
				}
				average = average/(data.length-1);
				students.add(new Student(name,""+average));
			}	
		}catch (FileNotFoundException e){
			System.err.println("Cannot read file "+file);
		}		catch (IOException e){
			System.err.println("Error reading from file");
		}		
	}

	public void writeFile(String file) {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file,true);
			PrintWriter out = new PrintWriter(fileWriter);
			
			out.println("--------- "+scorename+" ---------\nName Average\n==== =======");
			for (Student stu : students) {
				out.println(stu.getName()+"\t"+stu.getAverage());
			}
		}catch (IOException e) {
			System.err.println("");
		}finally {
			try {
				if (fileWriter != null)
					fileWriter.close();
			}catch(IOException e) {
				System.err.println("Error closing file");
			}
		}
	}
}