package Homework;

public class Student {
	private String name;
	private String average;
	
	public Student(String name,String average){
		this.name = name;
		this.average = average;
	}
	
	public String getName(){
		return name;
	}
	
	public String getAverage(){
		return average;
	}
}
