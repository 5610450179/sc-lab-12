package Phonebook;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class DataContact {
	private String name;
	private String phone;
	
	public DataContact(String name,String phone){
		this.name = name;
		this.phone = phone;
	}
	
	public String getName(){
		return name;
	}
	
	public String getPhone(){
		return phone;
	}

}
