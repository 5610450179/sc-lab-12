package Phonebook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBook {
	
	private ArrayList<DataContact> contacts = new ArrayList<DataContact>(); 
	
	public void readContact(String file){
		FileReader fileReader = null;
		try{
			fileReader = new FileReader(file);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line = buffer.readLine();
			while(line!=null){
				String[] contact = line.split(",");
				contacts.add(new DataContact(contact[0].trim(), contact[1].trim()));
				line = buffer.readLine();
			}
			
		}catch(FileNotFoundException e){
			System.err.println("can not read "+file);
		}catch(IOException e){
			System.err.println("Error readding file");
		}finally{
			if(fileReader != null){
				try{
					fileReader.close();
				}catch(IOException e){
					System.err.println("Error closing file");
				}
			}
		}
	}
	
	public void display(){
		System.out.println("--------- Java Phone Book ---------\nName Phone\n==== =====");
		for(DataContact ele:contacts){
			System.out.println(ele.getName()+"\t"+ele.getPhone());
		}
	}

}
